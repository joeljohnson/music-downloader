import configparser
import urllib.request
from typing import List

from gmusicapi.clients import Mobileclient
from eyed3.id3 import Tag, ID3_V2_4


def main():
    config = configparser.ConfigParser()
    config.read("config.ini")

    username = config.get("auth", "username")
    password = config.get("auth", "password")

    device_id = Mobileclient.FROM_MAC_ADDRESS
    if config.has_option("auth", "device_id"):
        device_id = config.get("auth", "device_id")

    client = Mobileclient()
    client.login(username, password, device_id)

    if device_id == Mobileclient.FROM_MAC_ADDRESS:
        devices = client.get_registered_devices()
        for device in devices:
            print(f'{device.get("friendlyName", device["type"])}: {device["id"]}')
        return

    playlist_name = config.get("download", "playlist_name")
    directory = config.get("download", "output_directory")
    remove_after_download = config.getboolean("download", "remove_after_download")

    playlists: List[dict] = client.get_all_user_playlist_contents()

    download_list = [pl for pl in playlists if pl['name'] == playlist_name][0]
    tracks_to_download: List[dict] = download_list['tracks']

    count = 0
    total = len(tracks_to_download)

    print(f"Found {total} songs to download")

    for track in tracks_to_download:
        count = count + 1
        url = client.get_stream_url(track['trackId'])

        filename = "unknown-{}.mp3".format(track['id'])
        meta = None
        if 'track' in track:
            meta = track['track']
            filename = "{artist}-{title}.mp3".format(**meta).replace('/', '-').replace('"', '').replace(' ', '_')

        print(f"{count}/{total} Downloading {filename}")
        urllib.request.urlretrieve(url, f"{directory}/{filename}")
        print("Done")

        if meta:
            tag = Tag()
            tag.artist = meta['artist']
            tag.album = meta['album']
            tag.title = meta['title']
            tag.track_num = (int(meta['trackNumber']), int(meta['trackType']))
            tag.save(f'{directory}/{filename}', ID3_V2_4)

        if remove_after_download:
            client.remove_entries_from_playlist(track['id'])


if __name__ == "__main__":
    main()
